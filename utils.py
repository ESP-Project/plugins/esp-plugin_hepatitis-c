from collections import defaultdict


def build_patient_event_dict(events):
    patient_events = defaultdict(list)
    for ev in events:
        patient_events[ev['patient']].append(ev)

    return patient_events


def extract_pks(patient_event_dict):
    return {patient: [event['pk'] for event in events] for patient, events in patient_event_dict.items()}


def populate_patient_analysis(patient_events):
    from data_structs import PatientAnalysis
    patient_analysis = []
    for key, elems in patient_events.items():
        patient_analysis.append(PatientAnalysis(elems))
    return patient_analysis


def reclassify_patient(criteria, events):
    from data_structs import PatientAnalysis
    return PatientAnalysis(
        events.values('pk', 'name', 'patient', 'patient__date_of_birth', 'date', 'case__condition'),
        criteria='acute_{}'.format(criteria.split(' ')[1].lower()))


def get_unbound_events(names, condition):
    return [e for e in get_events({'name__in': names},
                                  ['pk', 'name', 'patient', 'patient__date_of_birth', 'date', 'case__condition'])
            if not e['case__condition'] == condition]


def ignore_events_prior_to_case_date(events, condition):
    from ESP.nodis.models import Case
    patient_case_dates = {c['patient']: c['date'] for c in
                          Case.objects.filter(condition=condition).values('patient', 'date')}
    return [e for e in events if e['date'] >= patient_case_dates.get(e['patient'], e['date'])]


def get_events(filter, values, order='date'):
    from ESP.hef.models import Event
    return Event.objects.filter(**filter).order_by(order).values(*values)


def events_offset(one, two):
    return abs((one['date'] - two['date']).days)


def build_event_date_offset_map(offset_from, events):
    ed_map = defaultdict(list)
    for event in events:
        ed_map[events_offset(event, offset_from)].append(event)
    return ed_map
