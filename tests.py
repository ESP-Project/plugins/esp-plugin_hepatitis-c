import unittest
from datetime import datetime, date, timedelta

from data_structs import EligibleNegatedError, EventsNotFoundError, MissingRequiredError, MissingTriggerError, \
    MissingVerifyingError, DELAY_DAYS, AnalysisBase, AAnalysis, BAnalysis


class MockEvent(object):
    def __init__(self, name=None, patient=10, date=None, dob=date(1990, 1, 1)):
        import random
        import string
        self.pk = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(5))
        self.date = date
        self.name = name
        self.patient = patient
        self.patient__date_of_birth = dob

    def __getitem__(self, item):
        return self.__dict__[item]


class TestHepC(unittest.TestCase):
    def setUp(self):
        self.events = [
            MockEvent(name='lx', patient='1', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='rx', patient='1', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-03', '%Y-%m-%d')),
            MockEvent(name='lx', patient='2', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='rx', patient='2', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='dx', patient='2', date=datetime.strptime('2000-01-03', '%Y-%m-%d')),
        ]

        self.multiple_events_by_date = [
            MockEvent(name='lx', patient='1', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='rx', patient='1', date=datetime.strptime('2000-01-03', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='dx', patient='1', date=datetime.strptime('2000-01-05', '%Y-%m-%d')),
        ]

        self.simple_chronic = [
            [MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                       date=datetime.strptime('2000-01-01', '%Y-%m-%d'))],
            [MockEvent(name='lx:hepatitis_c_rna:positive', patient='1',
                       date=datetime.strptime('2000-01-01', '%Y-%m-%d'))],
            [MockEvent(name='lx:hepatitis_c_riba:positive', patient='1',
                       date=datetime.strptime('2000-01-01', '%Y-%m-%d'))],
            [MockEvent(name='lx:hepatitis_c_signal_cutoff:positive', patient='1',
                       date=datetime.strptime('2000-01-01', '%Y-%m-%d'))],
        ]

        self.multiple_chronic_date = [
            MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                      date=datetime.strptime('2000-01-05', '%Y-%m-%d')),
            MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                      date=datetime.strptime('2000-01-02', '%Y-%m-%d'))
        ]

        self.multiple_chronic_name = [
            MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                      date=datetime.strptime('2000-01-05', '%Y-%m-%d')),
            MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                      date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='lx:hepatitis_c_rna:positive', patient='1',
                      date=datetime.strptime('2000-01-01', '%Y-%m-%d'))
        ]

        self.c_trigger = [
            MockEvent(name='lx:hepatitis_c_rna:positive', patient='1',
                      date=datetime.strptime('2000-01-01', '%Y-%m-%d'))
        ]

        self.d_trigger = [
            MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                      date=datetime.strptime('2000-01-01', '%Y-%m-%d'))
        ]

        self.fail_cd_prior_positive = [
            [MockEvent(name='lx:hepatitis_c_riba:positive', patient='1',
                       date=datetime.strptime('1900-01-01', '%Y-%m-%d'))],
            [MockEvent(name='dx:hepatitis_c:unspecified', patient='1',
                       date=datetime.strptime('1900-01-01', '%Y-%m-%d'))]

        ]

        self.cd_prior_negative_good = [
            MockEvent(name='lx:hepatitis_c_elisa:negative', patient='1',
                      date=datetime.strptime('1999-02-02', '%Y-%m-%d')),
            MockEvent(name='lx:hepatitis_c_rna:negative', patient='1',
                      date=datetime.strptime('1999-02-02', '%Y-%m-%d'))
        ]

        self.cd_prior_negative_bad = [
            MockEvent(name='lx:hepatitis_c_elisa:negative', patient='1',
                      date=datetime.strptime('1998-02-02', '%Y-%m-%d')),
            MockEvent(name='lx:hepatitis_c_rna:negative', patient='1',
                      date=datetime.strptime('1998-02-02', '%Y-%m-%d'))
        ]

        self.positive_3 = MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                                    date=datetime.strptime('2000-01-01', '%Y-%m-%d'))

        self.positive_6 = MockEvent(name='lx:hepatitis_c_rna:positive', patient='1',
                                    date=datetime.strptime('1999-01-01', '%Y-%m-%d'))

        self.positive_6_b = MockEvent(name='lx:hepatitis_c_rna:positive', patient='1',
                                      date=datetime.strptime('2000-01-27', '%Y-%m-%d'))

        self.positive_1 = MockEvent(name='dx:jaundice', patient='1',
                                    date=datetime.strptime('2000-01-01', '%Y-%m-%d'))

        self.disconfirming_before = MockEvent(name='lx:hepatitis_c_riba:negative', patient='1',
                                              date=datetime.strptime('1999-12-15', '%Y-%m-%d'))

        self.not_disconfirming = MockEvent(name='lx:hepatitis_c_riba:negative', patient='1',
                                           date=datetime.strptime('1998-12-15', '%Y-%m-%d'))

        self.disconfirming_after = MockEvent(name='lx:hepatitis_c_rna:indeterminate', patient='1',
                                             date=datetime.strptime('2000-01-27', '%Y-%m-%d'))

        self.disconfirming_7 = MockEvent(name='dx:hepatitis_c:unspecified', patient='1',
                                         date=datetime.strptime('1999-12-31', '%Y-%m-%d'))

        self.disconfirming_8 = MockEvent(name='dx:hepatitis_c:chronic', patient='1',
                                         date=datetime.strptime('2000-01-01', '%Y-%m-%d'))

        self.not_prior_disconfirming = [
            MockEvent(name='dx:hepatitis_c:unspecified', patient='1',
                      date=datetime.strptime('2000-01-01', '%Y-%m-%d')),
            MockEvent(name='dx:hepatitis_c:chronic', patient='1',
                      date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
        ]

        self.multiple_acute_a_triggers = [
            MockEvent(name='dx:jaundice', patient='1',
                      date=datetime.strptime('2000-01-02', '%Y-%m-%d')),
            MockEvent(name='lx:bilirubin_total:threshold:gt:3', patient='1',
                      date=datetime.strptime('2000-01-15', '%Y-%m-%d')),
            MockEvent(name='lx:alt:threshold:gt:200', patient='1',
                      date=datetime.strptime('1999-12-22', '%Y-%m-%d')),
            MockEvent(name='lx:alt:threshold:gt:200', patient='1',
                      date=datetime.strptime('1999-12-10', '%Y-%m-%d'))
        ]

        self.disconfirming_acute = MockEvent(name='dx:hepatitis_c:chronic', patient='1',
                                             date=datetime.strptime('1900-01-01', '%Y-%m-%d'))

    def tearDown(self):
        pass

    def _get_analysis_from_pa(self, pa, klass=None):
        first_event = pa.first_event
        events = pa.events
        event_offset_map = pa.event_date_offset_map

        if klass is None:
            klass = AnalysisBase

        return klass(events, event_offset_map, first_event)

    def _run_analysis(self, analysis):
        analysis.run()
        return analysis

    def test_build_patient_event_dict(self):
        from utils import build_patient_event_dict
        patient_dict = build_patient_event_dict(self.events)
        expected = {'1': self.events[0:3],
                    '2': self.events[3:6]}

        self.assertDictEqual(patient_dict, expected)

    def test_patient_analysis_init(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis(self.events[0:3])
        self.assertEqual(pa.now, date.today())
        self.assertEqual(pa.patient, '1')
        self.assertEqual(pa.first_event, self.events[1])
        self.assertListEqual(pa.events, [self.events[1], self.events[0], self.events[2]])
        offset_dict = {1: [self.events[0]], 0: [self.events[1]], 2: [self.events[2]]}
        self.assertDictEqual(pa.event_date_offset_map, offset_dict)

    def test_mixed_patient_analysis_error(self):
        from data_structs import PatientAnalysis
        with self.assertRaises(ValueError):
            PatientAnalysis(self.events[0:4])

    def test_build_event_date_offset_map(self):
        from data_structs import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)
        offset_dict = {0: [evs[0], evs[2]], 1: [evs[3], evs[4]], 2: [evs[1]], 4: [evs[5]]}
        self.assertDictEqual(pa.event_date_offset_map, offset_dict)

    def test_events_offset(self):
        from utils import events_offset
        self.assertEqual(events_offset(self.events[1], self.events[2]), 2)
        self.assertEqual(events_offset(self.events[0], self.events[1]), 1)

    def test_events_offset_by_range(self):
        from data_structs import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)

        analysis = self._get_analysis_from_pa(pa)
        self.assertItemsEqual(analysis.get_events_by_offset_range(0, 3), [evs[0], evs[1], evs[2], evs[3], evs[4]])
        self.assertItemsEqual(analysis.get_events_by_offset_range(-22, 3), [evs[0], evs[1], evs[2], evs[3], evs[4]])
        with self.assertRaises(EventsNotFoundError):
            analysis.get_events_by_offset_range(3, 4)

    def test_get_events_by_name(self):
        from data_structs import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)
        analysis = self._get_analysis_from_pa(pa)
        self.assertDictEqual(analysis.get_events_by_names(['lx', 'rx', 'dx']),
                             {'lx': [evs[0]], 'rx': [evs[1]], 'dx': evs[2:]})
        self.assertDictEqual(analysis.get_events_by_names(['dx']), {'dx': evs[2:]})
        self.assertDictEqual(analysis.get_events_by_names(['dx'], analysis.get_events_by_offset_range(0, 3)),
                             {'dx': evs[2:5]})
        with self.assertRaises(EventsNotFoundError):
            analysis.get_events_by_names(['fail'])
        with self.assertRaises(EventsNotFoundError):
            analysis.get_events_by_names(['lx'], analysis.get_events_by_offset_range(1, 3))

    def test_get_first_events_by_names(self):
        from data_structs import PatientAnalysis
        evs = self.multiple_events_by_date
        pa = PatientAnalysis(evs)
        analysis = self._get_analysis_from_pa(pa)
        self.assertItemsEqual(analysis.get_first_events_by_names(['lx', 'rx', 'dx']), [evs[0], evs[1], evs[2]])
        self.assertItemsEqual(analysis.get_first_events_by_names(['lx', 'dx']), [evs[0], evs[2]])
        self.assertItemsEqual(
            analysis.get_first_events_by_names(['lx', 'dx'], analysis.get_events_by_offset_range(4, 6)), [evs[5]])

    def test_events_after_trigger(self):
        from data_structs import PatientAnalysis
        evs = [self.c_trigger[0], self.positive_3, self.positive_6, self.positive_6_b,
               self.disconfirming_before, self.disconfirming_after, self.disconfirming_7, self.not_disconfirming,
               self.disconfirming_acute, self.positive_1]
        pa = PatientAnalysis(evs)
        analysis = self._get_analysis_from_pa(pa)
        analysis.trigger = self.disconfirming_acute
        self.assertItemsEqual(analysis.get_events_after_trigger(),
                              [self.c_trigger[0], self.positive_3, self.positive_6, self.positive_6_b,
                               self.disconfirming_7, self.positive_1])

    def test_under_18_months(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis(self.events[0:1])
        self.assertFalse(pa.under_18_months(date(2000, 1, 1)))
        self.assertFalse(pa.under_18_months(date(1991, 7, 1)))
        self.assertTrue(pa.under_18_months(date(1991, 6, 30)))
        self.assertTrue(pa.under_18_months(date(1990, 2, 1)))

    def test_acute_or_unspecified(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis(self.multiple_events_by_date)
        self.assertEqual(pa.acute_or_unspecified(date.today()), 'HEP_C-U')
        self.assertEqual(pa.acute_or_unspecified(date.today() - timedelta(12)), 'HEP_C-U')
        self.assertEqual(pa.acute_or_unspecified(date.today() - timedelta(DELAY_DAYS + 1)), 'HEP_C-A')

    def test_chronic_eligible(self):
        from data_structs import PatientAnalysis
        for me in self.simple_chronic:
            pa = PatientAnalysis(me)
            self.assertEqual(self._run_analysis(pa.chronic_eligible()).trigger, me[0])
        pa = PatientAnalysis(self.multiple_chronic_date)
        self.assertEqual(self._run_analysis(pa.chronic_eligible()).trigger, self.multiple_chronic_date[1])

        pa = PatientAnalysis(self.multiple_chronic_name)
        self.assertEqual(self._run_analysis(pa.chronic_eligible()).trigger, self.multiple_chronic_name[2])

        pa = PatientAnalysis(self.events[0:3])
        with self.assertRaises(MissingTriggerError):
            pa.chronic_eligible().run()

    def test_acute_cd_eligible_false_on_event_key(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis(self.events[0:3])
        with self.assertRaises(MissingTriggerError):
            pa.acute_c_eligible().run()
        with self.assertRaises(MissingTriggerError):
            pa.acute_d_eligible().run()

    def test_acute_cd_eligible_false_prior(self):
        from data_structs import PatientAnalysis
        for ev in self.fail_cd_prior_positive:
            events = self.c_trigger
            events.extend(ev)
            pa = PatientAnalysis(events)
            with self.assertRaises(EligibleNegatedError):
                pa.acute_c_eligible().run()

    def test_acute_c_eligible(self):
        from data_structs import PatientAnalysis
        events = self.c_trigger
        events.extend(self.cd_prior_negative_good)
        pa = PatientAnalysis(events)
        self.assertEqual(self._run_analysis(pa.acute_c_eligible()).trigger, self.c_trigger[0])

    def test_acute_c_eligible_false_out_of_range(self):
        from data_structs import PatientAnalysis
        events = self.c_trigger
        events.extend(self.cd_prior_negative_bad)
        pa = PatientAnalysis(events)
        with self.assertRaises(MissingVerifyingError):
            pa.acute_c_eligible().run()

    def test_acute_d_eligible(self):
        from data_structs import PatientAnalysis
        events = self.d_trigger
        events.extend(self.cd_prior_negative_good)
        pa = PatientAnalysis(events)
        self.assertEqual(self._run_analysis(pa.acute_d_eligible()).trigger, self.d_trigger[0])

    def test_acute_d_eligible_false_out_of_range(self):
        from data_structs import PatientAnalysis
        events = self.d_trigger
        events.extend(self.cd_prior_negative_bad)
        pa = PatientAnalysis(events)
        with self.assertRaises(MissingVerifyingError):
            pa.acute_d_eligible().run()

    def test_ab_missing_event_key(self):
        from data_structs import PatientAnalysis
        with self.assertRaises(MissingTriggerError):
            pa = PatientAnalysis([self.positive_6])
            pa.acute_a_eligible().run()
        with self.assertRaises(MissingTriggerError):
            pa = PatientAnalysis([self.positive_3])
            pa.acute_b_eligible().run()

    def test_ab_missing_required(self):
        from data_structs import PatientAnalysis
        with self.assertRaises(MissingRequiredError):
            pa = PatientAnalysis([self.positive_3])
            pa.acute_a_eligible().run()
        with self.assertRaises(MissingRequiredError):
            pa = PatientAnalysis([self.positive_6])
            pa.acute_b_eligible().run()

    def test_acute_ab_negate_with_non_positive(self):
        from data_structs import PatientAnalysis
        from utils import events_offset
        pa = PatientAnalysis([self.positive_1, self.disconfirming_before])
        offset = events_offset(self.positive_1, pa.first_event)

        analysis = self._get_analysis_from_pa(pa, BAnalysis)
        self.assertIsNotNone(analysis.acute_ab_negate_with_non_positive(offset))

        pa = PatientAnalysis([self.positive_1, self.disconfirming_after])
        offset = events_offset(self.positive_1, pa.first_event)
        analysis = self._get_analysis_from_pa(pa, AAnalysis)
        self.assertIsNotNone(analysis.acute_ab_negate_with_non_positive(offset))

        pa = PatientAnalysis([self.positive_1, self.not_disconfirming])
        offset = events_offset(self.positive_1, pa.first_event)
        analysis = self._get_analysis_from_pa(pa, AAnalysis)
        self.assertIsNone(analysis.acute_ab_negate_with_non_positive(offset))

    def test_acute_ab_negate_with_prior(self):
        from data_structs import PatientAnalysis
        from utils import events_offset
        pa = PatientAnalysis([self.positive_3, self.positive_6])
        offset = events_offset(self.positive_3, pa.first_event)
        analysis = self._get_analysis_from_pa(pa, AAnalysis)
        self.assertEqual(analysis.acute_ab_negate_with_prior(offset), [self.positive_6])

        pa = PatientAnalysis([self.positive_3, self.disconfirming_7])
        offset = events_offset(self.positive_3, pa.first_event)
        analysis = self._get_analysis_from_pa(pa, BAnalysis)
        self.assertIsNotNone(analysis.acute_ab_negate_with_prior(offset))

        pa = PatientAnalysis([self.positive_3, self.disconfirming_8])
        offset = events_offset(self.positive_3, pa.first_event)
        analysis = self._get_analysis_from_pa(pa, AAnalysis)
        self.assertIsNotNone(analysis.acute_ab_negate_with_prior(offset))

        events = [self.positive_1]
        events.extend(self.not_prior_disconfirming)
        pa = PatientAnalysis(events)
        offset = events_offset(self.positive_3, pa.first_event)
        analysis = self._get_analysis_from_pa(pa, BAnalysis)
        self.assertListEqual(analysis.acute_ab_negate_with_prior(offset), [])

    def test_ab_eligible(self):
        from data_structs import PatientAnalysis
        events = [self.positive_1, self.positive_3]
        events.extend(self.not_prior_disconfirming)
        pa = PatientAnalysis(events)
        self.assertEqual(self._run_analysis(pa.acute_a_eligible()).trigger, self.positive_1)

        from data_structs import PatientAnalysis
        events = [self.positive_1, self.positive_6_b]
        events.extend(self.not_prior_disconfirming)
        pa = PatientAnalysis(events)
        self.assertEqual(self._run_analysis(pa.acute_b_eligible()).trigger, self.positive_1)

        from data_structs import PatientAnalysis
        events = [self.positive_3]
        events.extend(self.multiple_acute_a_triggers)
        events.extend(self.not_prior_disconfirming)
        pa = PatientAnalysis(events)
        self.assertEqual(self._run_analysis(pa.acute_a_eligible()).trigger, self.multiple_acute_a_triggers[3])

        from data_structs import PatientAnalysis
        events = [self.positive_3, self.disconfirming_acute]
        events.extend(self.multiple_acute_a_triggers)
        events.extend(self.not_prior_disconfirming)
        pa = PatientAnalysis(events)
        self.assertIsNone(self._run_analysis(pa.acute_a_eligible()).trigger)

    def test_find_case(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis(self.events[0:3])
        self.assertIsNone(pa.find_case())

        events = self.c_trigger
        events.extend(self.d_trigger)
        events.extend(self.cd_prior_negative_good)
        pa = PatientAnalysis(events)
        self.assertEquals(pa.find_case()[1], "Criteria C")

    def test_unspecified_should_be_before_chronic(self):
        from data_structs import PatientAnalysis
        positive_3 = MockEvent(name='lx:hepatitis_c_elisa:positive', patient='1',
                               date=date.today() - timedelta(15))

        positive_1 = MockEvent(name='dx:jaundice', patient='1',
                               date=date.today() - timedelta(4))

        pa = PatientAnalysis([positive_1, positive_3])
        self.assertEqual(pa.find_case()[0], 'HEP_C-U')

        positive_3.date = date.today() + timedelta(2)
        pa = PatientAnalysis([positive_1, positive_3])
        self.assertEqual(pa.find_case()[0], 'HEP_C-U')

        positive_3.date = date.today() - timedelta(40)
        pa = PatientAnalysis([positive_1, positive_3])
        self.assertEqual(pa.find_case()[0], 'HEP_C-C')

        positive_3.date = date.today() - timedelta(47)
        positive_1.date = date.today() - timedelta(35)
        pa = PatientAnalysis([positive_1, positive_3])
        self.assertEqual(pa.find_case()[0], 'HEP_C-A')

        pa = PatientAnalysis([positive_1, positive_3])
        pa.dob = date.today() - timedelta(400)
        self.assertEqual(pa.find_case()[0], 'HEP_C-C')

        positive_1.date = date.today() - timedelta(150)
        pa = PatientAnalysis([positive_1, positive_3])
        self.assertEqual(pa.find_case()[0], 'HEP_C-C')

    def test_chronic_gets_negated_ab_event(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis([self.positive_1, self.positive_3, self.disconfirming_after])

        status, criteria, analysis = pa.find_case()
        self.assertEqual(status, 'HEP_C-C')
        self.assertEqual(criteria, 'Criteria E')
        self.assertEqual(analysis.trigger, self.positive_3)
        self.assertIn(self.disconfirming_after, analysis.case_events)

    def test_get_evet_pk_list(self):
        from data_structs import PatientAnalysis
        pa = PatientAnalysis([self.positive_1, self.positive_6, self.positive_6_b])

        status, criteria, analysis = pa.find_case()

        self.assertItemsEqual({e['pk'] for e in analysis.case_events},
                              [self.positive_6_b.pk, self.positive_1.pk])

    def test_is_e_not_b(self):
        from data_structs import PatientAnalysis
        events = [
            MockEvent(name='lx:hepatitis_c_rna:positive', date=date(2010, 4, 12)),
            MockEvent(name='lx:alt:threshold:gt:200', date=date(2010, 4, 12)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2010, 5, 11)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2010, 5, 21)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2010, 9, 12)),
            MockEvent(name='lx:hepatitis_c_elisa:positive', date=date(2010, 4, 10)),
            MockEvent(name='lx:hepatitis_c_rna:indeterminate', date=date(2010, 4, 12)),
        ]

        pa = PatientAnalysis(events)
        self.assertEqual(pa.find_case()[1], "Criteria E")

    def test_is_chronic_not_a(self):
        from data_structs import PatientAnalysis
        events = [
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2000, 4, 26)),
            MockEvent(name='lx:alt:threshold:gt:200', date=date(2000, 4, 26)),
            MockEvent(name='lx:hepatitis_c_rna:negative', date=date(2000, 5, 24)),
            MockEvent(name='lx:hepatitis_c_rna:indeterminate', date=date(2000, 5, 24)),
            MockEvent(name='lx:hepatitis_c_elisa:positive', date=date(2000, 5, 24)),
            MockEvent(name='lx:hepatitis_c_rna:negative', date=date(2000, 5, 24)),
            MockEvent(name='lx:hepatitis_c_rna:indeterminate', date=date(2000, 5, 24)),
            MockEvent(name='lx:hepatitis_c_rna:positive', date=date(2000, 6, 6)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2000, 6, 6)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2000, 7, 1)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2000, 7, 8)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2000, 8, 1))
        ]

        pa = PatientAnalysis(events)
        self.assertEqual(pa.find_case()[1], "Criteria E")

    def test_is_chronic_not_b_due_to_prior_disconfirming(self):
        from data_structs import PatientAnalysis
        events = [
            MockEvent(name='lx:hepatitis_c_rna:positive', date=date(2007, 8, 12)),
            MockEvent(name='lx:alt:threshold:gt:200', date=date(2007, 8, 17)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2007, 8, 18)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2007, 8, 18)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2007, 8, 22)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2007, 11, 5)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2007, 11, 12)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2007, 11, 14)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2007, 11, 18)),
            MockEvent(name='dx:hepatitis_c:chronic', date=date(2000, 8, 20)),
            MockEvent(name='dx:hepatitis_c:unspecified', date=date(2000, 9, 24)),
        ]

        pa = PatientAnalysis(events)
        self.assertEqual(pa.find_case()[1], "Criteria E")

    def test_just_elisa_positive_recent(self):
        from data_structs import PatientAnalysis
        events = [
            MockEvent(name='lx:hepatitis_c_elisa:positive', date=date.today()-timedelta(4)),
        ]

        pa = PatientAnalysis(events)
        self.assertIsNone(pa.find_case())

    def test_just_elisa_positive_old(self):
        from data_structs import PatientAnalysis
        events = [
            MockEvent(name='lx:hepatitis_c_elisa:positive', date=date.today()-timedelta(29)),
        ]

        pa = PatientAnalysis(events)
        self.assertEqual(pa.find_case()[1], "Criteria E")

    def test_recent_unknown_holds_later_defined(self):
        from data_structs import PatientAnalysis
        events = [
            MockEvent(name='lx:hepatitis_c_elisa:positive', date=date.today()-timedelta(4)),
            MockEvent(name='lx:hepatitis_c_rna:positive', date=date.today()),
            MockEvent(name='lx:alt:threshold:gt:200', date=date.today() + timedelta(27)),
        ]

        pa = PatientAnalysis(events)
        self.assertIsNone(pa.find_case())


if __name__ == '__main__':
    unittest.main()
