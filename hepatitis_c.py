'''
                                  ESP Health
                                 Hepatitis C
                              Disease Definition


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org
@copyright: (c) 2011-2012 Channing Laboratory
@license: LGPL
'''

import datetime
from decimal import Decimal

from ESP.hef.base import DiagnosisHeuristic
from ESP.hef.base import Dx_CodeQuery
from ESP.hef.base import LabResultFixedThresholdHeuristic
from ESP.hef.base import LabResultPositiveHeuristic, LabResultAnyHeuristic
from ESP.hef.models import Event
from ESP.nodis.base import DiseaseDefinition
from ESP.nodis.models import Case, CaseActiveHistory
from ESP.utils import log
from django.db import IntegrityError
import os

from data_structs import ComponentMap
from utils import *

STATUS = {"HEP_C-A": "Acute", "HEP_C-U": "Unspecified", "HEP_C-C": "Chronic"}
CHANGE_REASON = {"HEP_C-I": "Initial", "HEP_C-UA": "Unspecified to Acute", "HEP_C-UC": "Unspecified to Chronic"}


class Hepatitis_C(DiseaseDefinition):
    '''
    Hepatitis C
    '''
    conditions = ['hepatitis_c']

    legacy_uris = ['urn:x-esphealth:disease:channing:hepatitis-combined:hepatitis_c:v1']
    uri = 'urn:x-esphealth:disease:channing:hepatitis_c:v1'

    short_name = 'hepatitis_c'

    test_name_search_strings = [
        'hep',
        'alt',
        'ast',
        'bili',
        'tbil',
        'hbv',
        'hcv',
        'sgpt',
        'sgot',
        'aminotrans',
        'genotype',
        'signal'
    ]

    timespan_heuristics = []

    def __init__(self, *args, **kwargs):
        super(Hepatitis_C, self).__init__(*args, **kwargs)

    @property
    def event_heuristics(self):
        heuristic_list = []
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_elisa',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_signal_cutoff',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_riba',
        ))
        heuristic_list.append(LabResultPositiveHeuristic(
            test_name='hepatitis_c_rna',
        ))
        heuristic_list.append(LabResultAnyHeuristic(
            test_name='hepatitis_c_genotype',
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_c:chronic',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B18.2', type='icd10'),
                Dx_CodeQuery(starts_with='070.54', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='hepatitis_c:unspecified',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='B19.20', type='icd10'),
                Dx_CodeQuery(starts_with='070.70', type='icd9'),
            ]
        ))
        heuristic_list.append(DiagnosisHeuristic(
            name='jaundice',
            dx_code_queries=[
                Dx_CodeQuery(starts_with='R17', type='icd10'),
                Dx_CodeQuery(starts_with='782.4', type='icd9'),
            ]
        ))
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='alt',
            threshold=Decimal('200'),
            match_type='gt',
        ))
        heuristic_list.append(LabResultFixedThresholdHeuristic(
            test_name='bilirubin_total',
            threshold=Decimal('3'),
            match_type='gt',
        ))

        return heuristic_list

    def generate_hepc(self):
        unbound_events = get_unbound_events(
            ComponentMap.make_set(statuses=['positive', 'negative', 'indeterminate']), self.conditions[0])
        new_or_no_case_events = ignore_events_prior_to_case_date(unbound_events, self.conditions[0])
        patient_events = build_patient_event_dict(new_or_no_case_events)
        self._update_event_dict_for_existing_cases(patient_events)
        new_cases = 0
        for patient in populate_patient_analysis(patient_events):
            new_cases += self._process_case(patient)

        return new_cases

    def update_unspecified_hepc(self):
        unspecified = Case.objects.filter(condition=self.conditions[0],
                                          criteria__in=['Criteria A', 'Criteria B'])

        unspecified = [u for u in unspecified if u.caseactivehistory_set.last().status == "HEP_C-U"]
        specified = 0
        for case in unspecified:
            reclassified = reclassify_patient(case.criteria, case.events.all()).find_case()
            if reclassified is None:
                raise RuntimeError("Previously classified case {} failed to reclassify".format(case))
            elif reclassified[0] == 'HEP_C-U':
                continue
            elif reclassified[0] == 'HEP_C-A':
                date = datetime.date.today()
                status = reclassified[0]
                event = Event.objects.get(pk=reclassified[2].trigger['pk'])
                change = "HEP_C-UA"
            elif reclassified[0] == 'HEP_C-C':
                case.criteria = reclassified[1]
                status = reclassified[0]
                event = Event.objects.get(pk=reclassified[2].negating_event['pk'])
                date = event.date
                change = "HEP_C-UC"

            case.status = 'RQ' if case.status in ['S', 'RS'] else case.status
            case.save()

            specified += CaseActiveHistory.update_or_create(
                case=case,
                status=status,
                date=date,
                change_reason=change,
                event_rec=event,
            )

        return specified

    def generate(self):
        log.info('Generating cases for %s (%s)' % (self.short_name, self.uri))
        generated = self.generate_hepc()
        log.info('Generated %s new cases for %s (%s)' % (generated, self.short_name, self.uri))
        log.info('Reclassifying unspecified cases for %s (%s)' % (self.short_name, self.uri))
        specified = self.update_unspecified_hepc()
        log.info('Reclassified %s new cases for %s (%s)' % (specified, self.short_name, self.uri))

        return generated

    def _update_event_dict_for_existing_cases(self, events):
        patients_with_existing_cases = self.add_events_to_existing_cases(events.keys(),
                                                                         [extract_pks(events)])
        pt_ids_for_existing_cases = set([p.pk for p in patients_with_existing_cases])

        for pid in pt_ids_for_existing_cases:
            events.pop(pid, None)

    def _process_case(self, patient):
        has_case = patient.find_case()
        if has_case is not None:
            trigger_event = Event.objects.get(pk=has_case[2].trigger['pk'])
            try:
                if has_case[0] not in STATUS:
                    raise ValueError()
                log.debug('Good Case for %s' % patient.patient)
                t, new_case = self._create_case_from_event_list(
                    condition=self.conditions[0],
                    criteria=has_case[1],
                    recurrence_interval=None,  # Does not recur
                    event_obj=trigger_event,
                    relevant_event_names=set([e['pk'] for e in has_case[2].case_events]) - {has_case[2].trigger['pk']},
                )
                CaseActiveHistory.create(
                    case=new_case,
                    status=has_case[0],
                    date=new_case.date,
                    change_reason="HEP_C-I",
                    event_rec=trigger_event,
                )
            except IntegrityError:
                log.debug('Case creation failed for     %s' % patient.patient)
                t = False
            except ValueError:
                log.debug('Case creation failed for     %s. Bad status %s' % patient.patient, has_case[0])
                t = False
            if t:
                log.info('Created new %s hepc case %s : %s' % (has_case[0], has_case[1], new_case))
                return 1
        return 0

    def report_field(self, report_field, case):
        reportable_fields = {
            'NA-56': 'NA-1739',
            'disease': 'NA-1561',
            '10187-3': 'NA-1738',
            'disease_status': self._get_disease_status(case),
            'symptom_obx': False,
            'na_trmt_obx': False,
        }

        return reportable_fields.get(report_field, None)

    def _get_disease_status(self, case):
        status = {'HEP_C-A': 'NAR-87', 'HEP_C-U': 'NAR-220', 'HEP_C-C': 'NAR-361'}
        return status.get(case.caseactivehistory_set.last().status, None)

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
# Packaging
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

def event_heuristics():
    return Hepatitis_C().event_heuristics


def disease_definitions():
    return [Hepatitis_C()]

