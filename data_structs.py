import abc
from collections import defaultdict, OrderedDict
from datetime import date, timedelta
from operator import itemgetter
import copy

from dateutil.relativedelta import relativedelta

from utils import events_offset, build_event_date_offset_map

DELAY_DAYS = 28


class ComponentMap(object):
    component_map = {
        1: 'dx:jaundice',
        2: 'lx:alt:threshold:gt:200',
        3: 'lx:hepatitis_c_elisa:{status}',
        4: 'lx:hepatitis_c_signal_cutoff:{status}',
        5: 'lx:hepatitis_c_riba:{status}',
        6: 'lx:hepatitis_c_rna:{status}',
        7: 'dx:hepatitis_c:unspecified',
        8: 'dx:hepatitis_c:chronic',
        9: 'lx:bilirubin_total:threshold:gt:3',
    }

    @classmethod
    def make_set(cls, keys=None, statuses=None):
        component_set = set()
        for key in keys or cls.component_map.keys():
            item = cls.component_map[key]
            if statuses is None:
                if '{status}' in item:
                    raise ValueError("parameter statuses must have string to format {}".format(item))
                else:
                    component_set.update([item])
            else:
                status_items = []
                for status in statuses:
                    status_items.append(item.format(status=status))
                component_set.update(status_items)

        return component_set


class AnalysisBase(object):
    def __init__(self, events, event_date_offset_map, first_event, **kwargs):
        self.trigger = None
        self.case_events = []
        self.negating_event = None
        self.first_event = first_event
        self.events = events
        self.event_date_offset_map = event_date_offset_map
        self.params = kwargs

    def get_events_by_offset_range(self, start=0, end=None, inclusive=False):
        events = []
        if end is None:
            end = max(self.event_date_offset_map.keys()) + 1
        elif inclusive:
            end += 1

        for offset in range(start, end):
            events.extend(self.event_date_offset_map.get(offset, []))
        if events:
            return events
        else:
            raise EventsNotFoundError

    def get_events_by_names(self, names, event_set=None):
        events = defaultdict(list)
        for event in event_set or self.events:
            if event['name'] in names:
                events[event['name']].append(event)

        if events:
            return events
        else:
            raise EventsNotFoundError

    def get_first_events_by_names(self, names, event_set=None):
        return sorted([vals[0] for key, vals in self.get_events_by_names(names, event_set).items()],
                      key=itemgetter('date'))

    def get_events_after_trigger(self):
        if self.trigger is not None:
            case_offset = events_offset(self.trigger, self.first_event)
            try:
                for name, events in self.get_events_by_names(ComponentMap.make_set(statuses=['positive']),
                                                             self.get_events_by_offset_range(
                                                                 start=case_offset + 1)).items():
                    self.case_events.extend(events)
            except EventsNotFoundError:
                pass

            return self.case_events


class Analysis(AnalysisBase):
    __metaclass__ = abc.ABCMeta

    def run(self):
        self.analyze(**self.params)
        self.get_events_after_trigger()

    @abc.abstractmethod
    def analyze(self, **kwargs):
        pass


class ABAnalysis(Analysis):
    def analyze(self):
        try:
            event = self.get_first_events_by_names(
                ComponentMap.make_set(keys=[self.event_key], statuses=['positive']))[0]
        except EventsNotFoundError:
            raise MissingTriggerError

        try:
            offset = events_offset(event, self.first_event)
            triggers = self.get_events_by_names(ComponentMap.make_set(keys=[1, 2, 9]),
                                                self.get_events_by_offset_range(offset - DELAY_DAYS,
                                                                                offset + DELAY_DAYS, inclusive=True))
        except EventsNotFoundError:
            if (date(event['date'].year, event['date'].month, event['date'].day) + timedelta(DELAY_DAYS)) < date.today():
                raise MissingRequiredError
            else:
                raise UnknownRequiredError(event)

        trigger_events = []
        for name, events in triggers.items():
            trigger_events.extend(events)

        trigger = sorted(trigger_events, key=itemgetter('date'))[0]
        offset = events_offset(trigger, self.first_event)
        self.negating_event = self.get_earlier_negating(offset, event)

        if self.negating_event is None:
            self.trigger = trigger
            self.case_events = [event]

    def get_earlier_negating(self, offset, event):
        negating = [self.acute_ab_negate_with_non_positive(offset)]
        negating.extend(self.acute_ab_negate_with_prior(offset))
        negating = [neg for neg in negating if neg is not None and not neg == event]

        if len(negating) == 0:
            return None
        else:
            return sorted(negating, key=itemgetter('date'))[0]

    def acute_ab_negate_with_non_positive(self, offset):
        negate_trigger_names = ComponentMap.make_set(keys=self.confirming_keys, statuses=['negative', 'indeterminate'])
        try:
            return self.get_first_events_by_names(negate_trigger_names,
                                                  self.get_events_by_offset_range(
                                                      offset - DELAY_DAYS,
                                                      offset + DELAY_DAYS, inclusive=True))[0]

        except EventsNotFoundError:
            return None

    def acute_ab_negate_with_prior(self, offset):
        negating = []
        try:
            negating.append(self.get_first_events_by_names(ComponentMap.make_set(keys=self.immediate_prior,
                                                                                 statuses=['positive']),
                                                           self.get_events_by_offset_range(end=offset))[0])
        except EventsNotFoundError:
            pass

        try:
            negating.append(
                self.get_first_events_by_names(ComponentMap.make_set(keys=self.delay_prior, statuses=['positive']),
                                               self.get_events_by_offset_range(end=offset - DELAY_DAYS))[0])
        except EventsNotFoundError:
            pass

        try:
            negating.append(self.get_first_events_by_names(ComponentMap.make_set(keys=[7]),
                                                           self.get_events_by_offset_range(end=offset))[0])
        except EventsNotFoundError:
            pass

        try:
            negating.append(self.get_first_events_by_names(ComponentMap.make_set(keys=[8]),
                                                           self.get_events_by_offset_range(end=offset, inclusive=True))[
                                0])
        except EventsNotFoundError:
            pass

        return negating


class AAnalysis(ABAnalysis):
    def __init__(self, *args, **kwargs):
        super(AAnalysis, self).__init__(*args, **kwargs)
        self.event_key = 3
        self.confirming_keys = [4, 5, 6]
        self.immediate_prior = [3]
        self.delay_prior = [5, 6]


class BAnalysis(ABAnalysis):
    def __init__(self, *args, **kwargs):
        super(BAnalysis, self).__init__(*args, **kwargs)
        self.event_key = 6
        self.confirming_keys = [4, 5]
        self.immediate_prior = [3, 6]
        self.delay_prior = [5]


class CDAnalysis(Analysis):
    def analyze(self, event_key=None):
        if None in (event_key,):
            raise ValueError("CDAnalysis requires the kwarg event_key")
        try:
            event = self.get_first_events_by_names(
                ComponentMap.make_set(keys=[event_key], statuses=['positive']))[0]
        except EventsNotFoundError:
            raise MissingTriggerError

        try:
            prior_event = self.get_first_events_by_names(
                ComponentMap.make_set(keys=[3, 5, 6, 7], statuses=['positive']))[0]
            if prior_event['date'] < event['date']:
                raise EligibleNegatedError
        except EventsNotFoundError:
            pass

        try:
            concurrent_prior_event = self.get_first_events_by_names(
                ComponentMap.make_set(keys=[8], statuses=['positive']))[0]
            if concurrent_prior_event['date'] <= event['date']:
                raise EligibleNegatedError
        except EventsNotFoundError:
            pass

        try:
            offset = events_offset(event, self.first_event)
            self.get_first_events_by_names(ComponentMap.make_set(keys=[3, 6], statuses=['negative']),
                                           self.get_events_by_offset_range(offset - 365, offset))
        except EventsNotFoundError:
            raise MissingVerifyingError

        self.trigger = event


class ChronicAnalysis(Analysis):
    def analyze(self):
        try:
            chronic_events = self.get_first_events_by_names(
                ComponentMap.make_set(keys=[3, 4, 5, 6], statuses=['positive']))
            self.trigger = chronic_events[0]
        except EventsNotFoundError:
            raise MissingTriggerError

    def add_events(self, events):
        self.case_events.extend(events)

    def set_negating(self, events):
        self.negating_event = sorted(events, key=itemgetter('date'))[0]


class PatientAnalysis(object):
    def __init__(self, events, criteria=None):
        self.events = sorted(events, key=itemgetter('date'))
        self.criteria = self._build_criteria(criteria)
        self.patient, self.dob = self._get_events_patient()
        self.first_event = self.events[0]
        self.event_date_offset_map = build_event_date_offset_map(self.first_event, self.events)
        self.now = date.today()

    def _get_events_patient(self):
        patient_list = set([(e['patient'], e['patient__date_of_birth']) for e in self.events])
        if not len(patient_list) == 1:
            raise ValueError(
                "PatientAnalysis instance got events for {} patients {}".format(len(patient_list), patient_list))
        return list(patient_list)[0]

    def _build_criteria(self, criteria=None):
        potential = OrderedDict()
        potential['chronic'] = self.chronic_eligible
        potential['acute_a'] = self.acute_a_eligible
        potential['acute_b'] = self.acute_b_eligible
        potential['acute_c'] = self.acute_c_eligible
        potential['acute_d'] = self.acute_d_eligible

        if criteria is None:
            return potential
        else:
            identified = OrderedDict()
            identified[criteria] = potential[criteria]
            identified['chronic'] = potential['chronic']

            return identified

    def find_case(self):
        negates = []
        criteria_copy = copy.deepcopy(self.criteria)
        for key, event_method in criteria_copy.items():
            try:
                analysis = event_method()
                analysis.run()
                if analysis.trigger is not None:
                    self.criteria[key] = analysis
                else:
                    del self.criteria[key]
                if analysis.negating_event is not None:
                    negates.append(analysis.negating_event)
            except (MissingRequiredError, MissingVerifyingError,
                    MissingTriggerError, EligibleNegatedError):
                del self.criteria[key]
            except UnknownRequiredError as e:
                del self.criteria[key]
                try:
                    if self.criteria['chronic'].trigger['date'] >= e.event['date']:
                        del self.criteria['chronic']
                except KeyError:
                    pass

        try:
            if negates:
                self.criteria['chronic'].set_negating(negates)
        except KeyError:
            pass

        return self.select_case_event_type([(key, analysis) for key, analysis in self.criteria.items()])

    def under_18_months(self, date):
        age = relativedelta(date, self.dob)
        if age.years == 0:
            return True
        if age.years == 1 and age.months < 6:
            return True
        return False

    def select_case_event_type(self, criteria_event):
        if not criteria_event:
            return None

        eligibles = sorted(criteria_event, key=lambda x: x[1].trigger['date'])
        event_types = []

        for eligible in eligibles:
            name, analysis = eligible[0], eligible[1]
            if name == "chronic":
                event_types.append(("HEP_C-C", 'Criteria E', analysis))
            else:
                if self.under_18_months(analysis.trigger['date']):
                    event_types.append(("HEP_C-C", 'Criteria F', analysis))
                elif name in ["acute_a", "acute_b"]:
                    event_types.append((self.acute_or_unspecified(analysis.trigger['date']),
                                        'Criteria {}'.format(name.split('_')[1].upper()), analysis))
                elif name in ["acute_c", "acute_d"]:
                    event_types.append(
                        ("HEP_C-A", 'Criteria {}'.format(name.split('_')[1].upper()), analysis))

        event_types = [e for e in event_types if
                       (e[2].trigger['date'] - eligibles[0][1].trigger['date']).days <= DELAY_DAYS * 2]

        if len(event_types) == 1:
            return event_types[0]

        a_u_events = [e for e in event_types if e[0] in ["HEP_C-U", "HEP_C-A"]]

        if len(a_u_events) > 0:
            return a_u_events[0]
        else:
            return event_types[0]

    def acute_or_unspecified(self, event_date):
        if (self.now - event_date).days > DELAY_DAYS:
            return "HEP_C-A"
        else:
            return "HEP_C-U"

    def acute_a_eligible(self):
        return AAnalysis(self.events, self.event_date_offset_map, self.first_event)

    def acute_b_eligible(self):
        return BAnalysis(self.events, self.event_date_offset_map, self.first_event)

    def acute_c_eligible(self):
        return CDAnalysis(self.events, self.event_date_offset_map, self.first_event, event_key=6)

    def acute_d_eligible(self):
        return CDAnalysis(self.events, self.event_date_offset_map, self.first_event, event_key=3)

    def chronic_eligible(self):
        return ChronicAnalysis(self.events, self.event_date_offset_map, self.first_event)


class EventsNotFoundError(Exception):
    pass


class MissingTriggerError(Exception):
    pass


class MissingRequiredError(Exception):
    pass


class UnknownRequiredError(Exception):
    def __init__(self, event):
        self.event = event


class MissingVerifyingError(Exception):
    pass


class EligibleNegatedError(Exception):
    pass
