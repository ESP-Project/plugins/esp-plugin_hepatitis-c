'''
                                  ESP Health
                                 Hepatitis C
                             Packaging Information


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory http://www.channing.harvard.edu
@contact: http://www.esphealth.org
@copyright: (c) 2011-2012 Channing Laboratory
@license: LGPL

'''

from setuptools import find_packages
from setuptools import setup

setup(
    name='esp-plugin_hepatitis-c',
    version='3.7',
    author='Jeff Andre',
    author_email='jandre@commoninf.com',
    description='Hepatitis C disease definition module for ESP Health application',
    license='LGPLv3',
    keywords='hepatitis c algorithm disease surveillance public health epidemiology',
    url='http://esphealth.org',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=[
    ],
    entry_points='''
        [esphealth]
        disease_definitions = hepatitis_c:disease_definitions
        event_heuristics = hepatitis_c:event_heuristics
    '''
)
